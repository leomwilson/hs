\documentclass[12pt]{article}

\usepackage[margin=0.75in]{geometry}
\usepackage{setspace}
\usepackage{textcomp}
\usepackage{graphicx}
\usepackage{float}
\usepackage[colorlinks=false, linktoc=all]{hyperref}

\usepackage[backend=biber]{biblatex}
\addbibresource{solaroven.bib}

\doublespacing

\title{Solar Oven Report}
\date{\today}
\author{Chad Bogan\\ \and Leo Wilson\\ \and Quintin Thompson\\ \and Rohit Chagam}

\begin{document}

\begin{singlespace}
\maketitle
\end{singlespace}

\abstract
This project showcases a simple, low-cost approach to a solar oven. The oven is designed to provide sufficient heat to cook small foodstuffs or items. When leaving the oven in the sun some of the energy was absorbed which increased the temperature of the food or whatever was inside the oven. Building the solar oven wasn’t very difficult but did require a decent amount of time to finish as well as having to be careful with how much money of resources we used.

\newpage
\tableofcontents
\newpage

\section{Introduction}
The goal of this project was to build and test a solar oven and then compare those results to a data table that had a projection for what the solar oven’s temperature would reach. To achieve this we had to learn how to use excel/google sheets. Within google sheets, we had to learn how to create an equation that could then be used within smaller tables throughout the entire sheet to get values. These values were then graphed and the point of intersection was what the projected temperature would be. After creating the google spreadsheet, we then built the solar oven. For this, we needed to have very exact measurements and angles. This is because we needed the solar oven to match what was put into the spreadsheet to get the most accurate projection possible. By doing this we learned how to build a google spreadsheet. We also learned that it takes very little materials at an extremely cheap cost and a little amount of time to build a solar oven. This solar oven is able to reach very high temperatures that are comparable to those temperatures reached by a regular oven. This is why solar ovens are so appealing for the developing world because all you need is sunlight and you can cook something just the same as in a regular oven but at a drastically cheaper price and by using reusable energy.

\section{Design Theory and Analytical Model}
Because the project was based on highly mathematical principles of heat transfer, a theoretical model can be created using known formulae. This model is useful in predicting the expected results of testing, and it can be designed such that any variable parameters can be easily changed to reflect physical changes to the solar oven itself as well as variations in its environment.
By predicting the performance of the oven quickly and without material expense, one can much more efficiently compare multiple designs and parameters. For instance, one might determine how a given increase in size could be expected to affect the cost efficiency (thermal output per dollar) of the solar oven, and accordingly make a more informed decision as to whether or not to use the larger design. Because the time required to calculate the predicted performance of solar oven designs with altered sets of parameters is minimal, most of the time being spent initially creating the model, the comparison of a wide variety of possible designs in order to determine an optimal solution is encouraged.
The greatest value of the theoretical model, however, is for comparison to empirical data, which can demonstrate the efficiency of the oven. For example, if the model, given correct parameters, predicted a final output of 150\textcelsius{} while the oven, during testing, heated to only 100\textcelsius{}, it would demonstrate a significant issue with the physical implementation -- that is, the design itself was effective, but it was in some way built incorrectly, thereby causing it to underperform. Without the model, it would be impossible to determine whether the design itself was faulty or whether it was simply an error of construction. The information contained in this section is derived primarily from the resources provided for this project\cite{basics} \cite{vid}.

Energy is transeferred to, from, and within the solar oven through each of the three modes of heat transefer. Thermal energy is lost primarily as a result of conductive heat transfer between the walls of the chamber and the air and insulation material around it, while it is gained through radiation from the sun, which is converted to heat by the walls of the inner chamber. This process is most effective when those walls are black, as black materials absorb more light (the specifics of the effect of the color of the inner chamber's walls will be explored in greater detail later).

Convection does not meaningfully impact the model for the solar oven, as it is primarily concerned with behavior internal to the system and therefore negiligible for the purposes of predicting performance. However, it does play a significant role in the internal dynamics of the solar oven. Importantly, it helps transfer heat from the walls of the solar oven's inner chamber to other parts by; if the oven were used to cook an actual food item, this circulation of hot air would aid in evenly heating the oven's contents, creating a better final product. This convection would actually cause heat to be removed from the oven more rapidly if air from the chamber was allowed to flow freely between it and the outside. To prevent this, the inner chamber must be properly sealed, keeping hot air contained and preventing the loss of heat through convection.

Additionally, convection plays a major role in determining the effectiveness of the oven's insulation. Air, on its own, is a good insulator against conductive heat transfer, which is why the insulation of the outer chamber of the solar oven, which is comprised of packed newspaper, has large amounts of air contained within it. However, if the air was allowed to flow, it would experience the same form of convection as the air in the inner chamber. This would increase heat transfer out of the inner chamber, thereby reducing the efficiency of the oven. To reduce convection within the outer chamber, airflow must be restricted. This is accomplished through the use of the aforementioned newspaper, which creates small pockets of air largely seperated from each other, preventing them from undergoing significant convection. Interestingly, it is under this same basic principle that aerogel is such an effective insulator\cite{aero}.

The model is fundamentally concerned with determining the point at which the solar oven reaches a state of "thermal equilibrium," wherein the thermal energy added to the chamber due to solar radiation is equal to the energy being lost to the oven's surroundings.
Because the total energy neither decreases nor increases, the thermal energy (and by extension the temperature as variables such as mass which would affect the relationship between temperature and thermal energy are assumed to be constant) of the chamber does not change. Since this deals with the rate of change of energy (energy itself increases, hence the increased temperature of the chamber), the relationship can be given by the following equation, where \(P = \mbox{power}\):
\[P_{out} = P_{absorbed}\]

Therefore, to determine the point of thermal equilibrium, the model must determine the values of \(P_{out}\) and \(P_{absorbed}\). Since the former depends on the latter (a hotter object will lose energy more quickly, in part because materials such as mylar have a temperature-dependent coefficient of heat transfer), the power absorbed by the oven must be determined first.

Solar radiation is measured in \(\mbox{W}/\mbox{m}^2\). In order to determine the power, the solar radiation should be multiplied by the area of the oven's window (which can be found by multiplying its length by its width), giving a preliminary result for the power in watts of the sun incident to the window.
However, this assumes that the window is perpendicular to the sun's rays, which is not necessarily the case. As such, to account for any angle, the following expression gives effective area of the window exposed to the sun, where \(\theta _s\) is the angle of the sun's rays relative to the ground and \(\beta\) is the angle of the window relative to the ground:
\[W * L * \sin (\theta _s + \beta)\]
Since \(\sin\) is maximized at an angle of 90deg (returning a value of 1, therefore using the full area of the window), an optimal result is achieved when \(\theta _s + \beta = 90\). In practice, this means that the oven should be oriented such that the window is perpendicular to the sun's rays.
Putting all these expressions together, the power incident to the window of the solar oven is given by the following equation:
\[P_{incident\mbox{ }to\mbox{ }window} = I_o * W * L * \sin (\theta _s + \beta)\]
where \(I_o\) is the incident solar power density.

To determine the power actually transmitted into the chamber through the window requires that the incident power is multiplied by the optical transmission of the window. To determine this, the expression \(\tau ^n\) is used, where \(\tau\) is the optical transfer coefficient of the material and \(n\) is the number of layers. This gives the equation
\[P_{transmitted} = \tau ^n * P_{incident\mbox{ }to\mbox{ }window}\]

Finally, the equation must take into account the absorbtion of the walls of the chamber. As mentioned, darker colors absorb more radiation to be converted into heat, and will thus have an absorbtion coefficient closer to 1. When this value is multiplied by the existing expressions, a final equation for the absorbed power can be determined:
\[P_{absorbed} = I_o * \tau ^n * \alpha * L * W * \sin (\theta _s + \beta)\]

Next, the power transfer from the oven to its surroundings must be determined. A general equation for such power transfer is as follows, where \(U\) is the combined heat transfer coefficient (which accounts roughly for each of the three modes of heat transfer and is measured in \(\mbox{W} / \left(\mbox{m}^2 * \mbox{\textcelsius}\right)\), \(A\) is the area of contact (\(\mbox{m}^2\)), and \(T\) is temperature in \textcelsius :
\[P_{out} = U * A * (T_{surface} - T_{receiving\mbox{ }surface})\]
The units triavially reduce to give an output in watts.

However, this equation is overly simplistic, and the model should instead seperate the equation into individual calculations for the components of the chamber, specifically the walls (including the bottom) and the window. This gives the equation:
\[P_{out} = \left(U_{sb} * A_{sb} + U_w * A_w\right)\left(T_{io} - T_{ambient}\right)\]
where \(U_{sb}\) is the overall heat transfer coefficient of the walls and bottom of the chamber, \(U_w\) is that of the window, \(A_{sb}\) and \(A_w\) are the areas of each respectively, \(T_{io}\) is the temperature of the chamber, and \(T_{ambient}\) is the ambient temperature in which the oven is being operated.

To determine the value of \(U_{sb}\), the model combines multiple layers using a generalized equation:
\[\frac{1}{U} = \sum_{i=n}^i \frac{x_i}{k_i}\]
where \(x\) is the thickness of each material, \(k\) is its thermal conductivity, and \(n\) is the number of layers. The thickness of cardboard was determined by the particular cardboard used, while the "thickness" of the newspaper (rather, the directional space taken up by the newspaper as opposed to the thickness of a particular sheet) is determined by the overall dimensions of the oven. The thermal conductivity for each has been established experimentally and was provided by the project specifications.

Given that the overall goal of the model is to find some value of \(T_{io}\) such that \(P_{out} = P_{absorbed}\), their respective equations can be set equal to each other and set in terms of \(T_{io}\):
\[T_{io} = T_{ambient} +
  \frac{I_o * L * W * \tau ^n * \alpha * \sin \left(\theta _s + \beta \right)}
  {U_{sb} * A_{sb} + U_w * A_w}\]

As previously mentioned, the mylar chosen for the window of the oven's internal chamber has a coefficient of heat transfer which depends on temperature. This relationship has been tested experimentally and a table showing values of \(U_w\) for given values of \(T_{io}\) was provided with the specifications for this project. In order to determine the temperature sufficient to cause the mylar window to induce thermal equilibrium (that is, the final temperature of the chamber), the equation for \(T_{io}\) must be compared to the equation for \(U_w\). For the latter, regression analysis can be used to determine an approkimate curve using the given empirical data (\(U_w\) being the x-value and \(T_{io}\) being the y-value). For the former, since all other data are known (either as constants or as design variables), \(U_w\) forms the independent variable and \(T_{io}\) once again serves as the dependent variable.Since the y-variable of both curves is \(T_{io}\), the y-value of their intersection point provides a final estimate for the resultant temperature of the oven's chamber given certain design parameters.

This model covers the basic solar oven design. However, the reflectors, which improve the performance of the oven, require a slight addition. Specifically, since the flat reflectors used for this project increase the amount of sunlight "seen" by the window rather than amplifying \(I_o\) (unlike curved reflectors, which would do so), it simply multiplies the power absorbed by a certain gain value. Where \(G\) represents this gain, the model is thus updated:
\[T_{io} = T_{ambient} +                                                                    
  \frac{I_o * L * W * G * \tau ^n * \alpha * \sin \left(\theta _s + \beta \right)}
  {U_{sb} * A_{sb} + U_w * A_w}\]

For the sake of simplicity, our calculation of gain assumes that the oven is positioned optimally such that \(\theta _s + \beta = 90\). Based on this assumption, the model can be slightly simplified:
\[T_{io} = T_{ambient} +                                                                    
  \frac{I_o * L * W * G * \tau ^n * \alpha}
  {U_{sb} * A_{sb} + U_w * A_w}\]

Gain depends on multiple factors, including the ratio of the reflector's length (\(M\)) to that of the oven's window, the empirical reflectivity of the mirrors (\(r\)), and the angle of incidence of the sun's rays relative to the mirrors (\(\alpha\)), which depends on \(M/L\) (common values were provided with the project specifications), as well as the number of reflectors used (\(N\)). For square reflectors, the gain can be calculated using the following equation:
\[G = 1 + N * r * \frac{M}{L} * \sin \alpha\]
Trapezoidal reflectors such as the ones used in this project, however, require a more complex equation for gain:
\[G = 1 + N * r * \frac{M}{L} * \left(\sin \alpha\right)
  * \left(1 + \frac{M}{L} * \sin \alpha\right)\]

Due to the length of each equation, this will not be combined with the equation for \(T_{io}\). However, these two equations together provide a final model for a solar oven with reflectors, which can be used in the same manner as the model without reflectors.

\section{Design Requirements}
This project was very open-ended and designed to allow unique solutions. As such, few constraints were explicitly defined. However, the cooking chamber was required to be able to hold the provided thermometer in order for the oven to be tested. While that may have been the only formal constraint of the project, many more design requirements were implicit. For example, the total size of the oven was limited by practicality, as the oven must be reasonably portable and should not use more materials than could easily be acquired at a relatively low cost. Furthermore, a significant imbalance in weight distribution or an otherwise awkward design would render it difficult to transport the oven for testing and storage. Given that certain materials (specifically the thermometer, black construction paper, and mylar) were provided, designs that used alternatives would require additional research, thus requiring more development time while also increasing the cost. Finally, since the provided documentation only covers a specific category of designs - that is, those in which the main body and inner chamber are rectangular prisms insulated from each other by newspaper - it would create additional and unnecessary difficulty to use something fundamentally different.

\section{Design Description}
The design I had was a simple design following the requirements needed. I got the dimensions from a box made previously and included the cooking chamber, the insulation, which was numerous pieces of newspaper, the mylar top, and the trapezoidal reflectors. In my design I made the cooking chamber much smaller than the actual box, to allow for more insulation and possibly get a higher temperature. I then added the reflectors to my solar oven to get an even higher temperature. When testing I looked up at where the sun was and then used wood blocks to set the oven at the right angle to get the maximum amount of sunlight.

\section{Design Justification}
I chose this overall design because I thought that it would result in a high temperature for my oven. The tall reflectors matched with the large insulating chamber would result in the most heat applied to the chamber and the most heat kept within it. For the exact dimensions, these were pretty much just random but were derived from a very general box design that was relatively large but still easy to transport and test. As for the final oven, the only thing I changed was adding the reflectors. I did notice that when I added the reflectors it sealed the chamber so that nothing could be put in unless the reflectors were removed. This is not a big issue and has no effect on the final oven temperature, but does interfere with any testing desired for actual food. Another thing that was decided was the size of the reflectors. I based the ratio and angles off of the spreadsheet, which gave the relative angles based on the ratio used. This allowed for the most sunlight to reach the chamber and raise the overall temperature. Using the largest ratio with a relatively large chamber window allowed for the reflectors to be the largest they can be for my model and raised the temperature even more. The heat transfer for our cardboard was .123 W/m and the newspaper was .64 W/m.

\section{Test Procedure}
The solar oven was tested anywhere from 9:30 to 10:30 in the morning. It was always tested during sunny days because when it was overcast the oven really couldn’t get that hot because of the lack of sunlight. Before testing the temperature probe was placed inside the oven chamber. The oven was then placed outside and aligned with the sun. Then using wood blocks the solar oven was propped up at an angle to get the maximum amount of sunlight hitting the oven chamber. Another temperature probe was used to measure the temperature outside because that would affect how hot the oven could get. The oven would be left outside for about 30 minutes, and then the data would be recorded in celsius. The data was recorded from the temperature probe inside the oven chamber and the temperature would also be taken from a different temperature probe to see how hot or cold it was outside. Lastly, this data was compared with the data table’s prediction for how hot the oven would be.

\section{Test Results}
For the first test, the model showed a predicted temperature of 96 degrees celsius. This was for a double window with my design specifications. On the first test however my final recorded temperature was 79 degrees celsius.

\begin{figure}[h!]
  \centering
  \includegraphics[width=5in]{solaroven-results-graph.png}
  \caption{Predicted results based on the model}
\end{figure}

We initially set up the oven in the sun-facing it directly and then left it to sit for about 30 minutes until the solar radiation reached a constant of 975 W/m. After this, we set up the temperature probes on the outside of the oven and in the chamber. This test was pretty perfect in terms of overall testing conditions, the sun was out, there was no wind, and the sun was directly overhead. For the reflector test, my oven predicted a temperature of 175 degrees celsius. This was using the gain equation, which initially gave me a reading of 236, but this is pretty much unheard of. During my test, it was also sunny with no wind and direct sunlight. As for the actual reading, the oven reached an internal temperature of 168 degrees celsius, which was close to the predicted, but not quite. The oven was left out for around 30-45 minutes to ensure the temperature would level off. I believe the factor that prohibited the final oven from reaching its predicted temperature was the way the box was built. In order to insert the temperature probes I had to cut a small square from the outside of the box and a smaller box from the inside of the chamber. This most likely allowed some heat to escape and thus made the overall temperature lower than predicted.

\section{Team Dynamics}
\begin{itemize}
  \item[Chad Bogan] Built a spreadsheet and built a solar oven, tested and gathered data, did a Cover page, Table of Contents, Introduction, Test procedure, Design Critique and Summary, and Team Dynamics.
  \item[Leo Wilson] Built a spreadsheet and built a solar oven, tested and gathered data, did Design Requirements, Design Theory, and Analytical Model, created SolidWorks model of the solar oven, organized the final draft of the report in {\LaTeX}, References
  \item[Quintin Thompson] Built a spreadsheet and built a solar oven that the report is based on, tested and gathered data, did design Description, Design Justification, Test results, Design Critique, and Summary.
  \item[Rohit Chagam] Made spreadsheet, built solar oven, tested and gathered data, did the Solar Oven Performance Index details, Appendices, Abstract
\end{itemize}

\section{Design Critique and Summary}
In the future, we should have worked as a group more. Everyone built and tested the solar ovens on their own because of covid and social distancing. This however made it more difficult because we really couldn’t communicate with each other. We all built a solar oven based on the same model.  However, we decided to base the report on Quintin’s oven because it was the most similar to the model out of everyone. If another group was to do this in the future just build one solar oven as a group so it is not as time-consuming and so you have only one spreadsheet. This will also help with building the oven because there is more than one person to check it and make sure there are no mistakes. Lastly, I would say getting all the materials before or as early as possible is super important because we were always looking for materials and it made this take longer because certain people didn’t have the necessary materials. Some critiques with my design were that it was very large, after the reflectors were added it was hard to get it through doorways and I had to be very cautious in order to not break it. This may have increased the temperature output but overall it was unpleasant having to transport it. Another downside to my design was that it did not provide adequate access for the temperature probes, and in order to get a reading I had to compromise the insulation of the box. This probably resulted in the lower oven temperature as was shown in my tests. Overall, I would tweak a few things just to make the oven less rough around the edges and make it perform better.

\printbibliography

\appendix
\section{Performance Index}
\begin{figure}[h!]
  \centering
  \includegraphics[width=5in]{solaroven-cost-breakdown.png}
  \caption{Cost breakdown of the solar oven}
\end{figure}

Transportation cost: .3lb * \$0.20/lb = \$0.06

Labor cost: \$5 (fixed)

Total oven cost: \$9.18

Performance index: \(T_{ambient}\) = 26.4 \textcelsius,
\(T_{actual}\) = 168 \textcelsius,
Cost-Based PI = \$15.42,
SOTD PI = -28

\section{Theoretical Results}
\begin{figure}[H]
  \centering
  \includegraphics[width=5in]{solaroven-model.png}
  \caption{Theoretical results from our model}
\end{figure}

\section{SolidWorks Models}
\begin{figure}[h!]
  \centering
  \includegraphics[width=2.5in]{solaroven-iso.png}
  \caption{Isometric view}
\end{figure}

\begin{figure}[h!]
  \centering
  \includegraphics[width=2.5in]{solaroven-side.png}
  \caption{Side view}
\end{figure}

\begin{figure}[h!]
  \centering
  \includegraphics[width=2.5in]{solaroven-top.png}
  \caption{Top view}
\end{figure}

\begin{figure}[h!]
  \centering
  \includegraphics[width=2.5in]{solaroven-bottom.png}
  \caption{Bottom view}
\end{figure}

\end{document}
